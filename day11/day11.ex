defmodule Day11 do
  @neighbours [%{x: -1, y: -1}, %{x: 0, y: -1}, %{x: 1, y: -1}, %{x: -1, y: 0}, %{x: 1, y: 0}, %{x: -1, y: 1}, %{x: 0, y: 1}, %{x: 1, y: 1}]

  def part1(steps) do
    data = readData()
    Enum.reduce(1..steps, {data,0}, fn x, acc ->
      step(acc, x)
    end)
    |> print()

  end

  defp print({data, c}) do
    Enum.each(0..9,fn y ->
      Enum.each(0..9, fn x ->
        IO.write(Map.get(data,{x,y}))
      end)
      IO.write("\n")
    end)
    IO.puts(c)
  end

  defp step({data, c}, step) do
    {flashed, newcounter} = Enum.map(data,fn {k,v} -> {k, v + 1} end) |> Map.new()
    |> flash(c)
    newdata = Enum.map(flashed, fn {k,v} -> if v < 0, do: {k, 0}, else: {k,v}  end) |> Map.new()
    if(Enum.sum(Map.values(newdata)) == 0, do: IO.puts("Fullflash detected: " <> Integer.to_string(step)))
    {newdata, newcounter}
  end

  defp flash(data, counter) do
    flashes = Enum.filter(data,fn {k, v} -> v > 9 end)
    if length(flashes) > 0 do
      Enum.reduce(flashes ,data,fn {k,_v}, acc ->
        Enum.reduce(@neighbours, Map.put(acc, k , -99999), fn x, acc2 ->
          coordinateTuple = {elem(k,0) + x.x, elem(k,1) + x.y}
          value = Map.get(acc2, coordinateTuple)
          if value != nil, do: Map.put(acc2, coordinateTuple , value + 1), else: acc2
        end)
      end)
      |> flash(counter + length(flashes))
    else
      {data,counter}
    end
  end



  defp readData() do
    {:ok, contents} = File.read("input")
    contents |> parse
  end

  defp parse(data) do
    result = String.split(data,"\n",trim: true)
    |> Enum.map(fn x ->
      String.codepoints(x)
      |> Enum.chunk_every(1)
      |> Enum.map(fn y ->
        String.to_integer(to_string(y))
      end)
    end)
    |> Enum.reduce(%{coordinateMap: %{}, counter: 0}, fn row, acc ->
      c = Enum.reduce(row,%{coordinateMap: acc.coordinateMap, counter: 0}, fn col, acc2 ->
        %{coordinateMap: Map.put(acc2.coordinateMap, {acc2.counter, acc.counter}, col), counter: acc2.counter + 1}
      end)
      %{coordinateMap: c.coordinateMap, counter: acc.counter + 1  }
    end)
    result.coordinateMap
  end
end


Day11.part1(100)
Day11.part1(1000)
