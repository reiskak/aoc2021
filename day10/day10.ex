defmodule Day10 do
  def part1() do
    data = readData()
    |> Enum.reduce(0, fn row, counter ->
      res = String.graphemes(row)
      |> Enum.reduce_while([], fn char, acc ->
        case char do
          n when n in ["{", "[", "(", "<"] -> {:cont,[char] ++ acc}
          "}" ->  if hd(acc) == "{", do: {:cont, tl(acc)}, else: {:halt, 1197}
          "]" ->  if hd(acc) == "[", do: {:cont, tl(acc)}, else: {:halt, 57}
          ")" ->  if hd(acc) == "(", do: {:cont, tl(acc)}, else: {:halt, 3}
          ">" ->  if hd(acc) == "<", do: {:cont, tl(acc)}, else: {:halt, 25137}
        end
      end)

      if is_integer(res), do: counter + res, else: counter
    end)
    |> IO.inspect()
  end

  def part2() do
    data = readData()
    |> Enum.reduce([], fn row, scores ->
      res = String.graphemes(row)
      |> Enum.reduce_while([], fn char, acc ->
        case char do
          n when n in ["{", "[", "(", "<"] -> {:cont,[char] ++ acc}
          "}" ->  if hd(acc) == "{", do: {:cont, tl(acc)}, else: {:halt, []}
          "]" ->  if hd(acc) == "[", do: {:cont, tl(acc)}, else: {:halt, []}
          ")" ->  if hd(acc) == "(", do: {:cont, tl(acc)}, else: {:halt, []}
          ">" ->  if hd(acc) == "<", do: {:cont, tl(acc)}, else: {:halt, []}
        end
      end)
      |> Enum.reduce(0, fn x, score ->
        case x do
          "{" -> (score*5) + 3
          "[" -> (score*5) + 2
          "(" -> (score*5) + 1
          "<" -> (score*5) + 4
        end
      end)
      if res > 0, do: scores ++ [res], else: scores
    end)
    |> Enum.sort()

    Enum.at(data,div(length(data), 2))
    |> IO.inspect()
  end

  defp readData() do
    {:ok, contents} = File.read("input.txt")
    contents |> parse
  end

  defp parse(data) do
    String.split(data,"\n",trim: true)
  end
end

Day10.part1()
Day10.part2()
