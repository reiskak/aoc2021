defmodule Day9 do
  @neighbours [%{x: -1, y: 0}, %{x: 0, y: -1}, %{x: 1, y: 0}, %{x: 0, y: 1}]
  def part1() do
    data = readData()
    getLowpoints(data.coordinateMap)
    |> Map.values()
    |> Enum.map(fn x -> x + 1 end)
    |> Enum.sum()
    |> IO.inspect()
  end

  def part2() do
    data = readData()
    getLowpoints(data.coordinateMap)
    |> Enum.map(fn {k,v} ->
      MapSet.size(mapBasin(k,data.coordinateMap, MapSet.new()))
    end)
    |> Enum.sort()
    |> Enum.take(-3)
    |> Enum.reduce(fn x, acc ->
      x * acc
    end)
    |> IO.inspect()

  end

  defp getLowpoints(coordinateMap) do
    Enum.filter(coordinateMap, fn {k,v} ->
      smallerNB = Enum.find(@neighbours, fn nb ->
        Map.get(coordinateMap,{elem(k,0) + nb.x, elem(k,1) + nb.y}, 100) <= v
      end)
      if smallerNB == nil, do: true, else: false
    end)
    |> Map.new()
  end

  defp mapBasin(lpCoords, coordinateMap, alreadyMapped) do
    basin = Enum.reduce(@neighbours, MapSet.new(), fn nb, acc ->
      nbt = {elem(lpCoords,0) + nb.x, elem(lpCoords,1) + nb.y}
      case Map.get(coordinateMap, nbt, 100) do
        100 -> acc
        9 -> acc
        _ -> if MapSet.member?(alreadyMapped, nbt), do: acc, else: MapSet.put(acc,nbt)
      end
    end)
    Enum.reduce(basin, MapSet.union(alreadyMapped,basin), fn x, acc ->
      MapSet.union(acc, mapBasin(x, coordinateMap, acc))
    end)
  end

  defp readData() do
    {:ok, contents} = File.read("input")
    contents |> parse
  end

  defp parse(data) do
    result = String.split(data,"\n",trim: true)
    |> Enum.map(fn x ->
      String.codepoints(x)
      |> Enum.chunk_every(1)
      |> Enum.map(fn y ->
        String.to_integer(to_string(y))
      end)
    end)
    |> Enum.reduce(%{coordinateMap: %{}, counter: 0, maxX: 0}, fn row, acc ->
      c = Enum.reduce(row,%{coordinateMap: acc.coordinateMap, counter: 0}, fn col, acc2 ->
        %{coordinateMap: Map.put(acc2.coordinateMap, {acc2.counter, acc.counter}, col), counter: acc2.counter + 1}
      end)
      %{coordinateMap: c.coordinateMap, counter: acc.counter + 1 , maxX: c.counter }
    end)
    %{coordinateMap: result.coordinateMap, dimensions: %{x: result.maxX - 1, y: result.counter - 1}}
  end
end

Day9.part1()
Day9.part2()
