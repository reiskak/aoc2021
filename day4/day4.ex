defmodule Day4 do
  def part1() do
    {:ok, contents} = File.read("input")
    data = contents |> parse

    winningBoard = Enum.reduce_while(data.numbers,data.boards,fn x, acc ->
      updatedBoards = markNumbers(acc,x)
      winners = checkWinners(updatedBoards)
      if winners != [] , do: {:halt, %{winner: List.first(winners), number: x}}, else: {:cont, updatedBoards}
    end)
    calculateScore(winningBoard.winner) * String.to_integer(winningBoard.number)
    |> IO.inspect()
  end

  def part2() do
    {:ok, contents} = File.read("input")
    data = contents |> parse

    lastWinningBoard = Enum.reduce_while(data.numbers,data.boards,fn x, acc ->
      updatedBoards = markNumbers(acc,x)
      winners = checkWinners(updatedBoards)
      cond do
        winners != [] and length(updatedBoards) == 1 ->  {:halt, %{winner: List.first(winners), number: x}}
        winners != [] -> {:cont, updatedBoards -- winners}
        true -> {:cont, updatedBoards}
      end
    end)
    calculateScore(lastWinningBoard.winner) * String.to_integer(lastWinningBoard.number)
    |> IO.inspect()
  end


  defp calculateScore(board) do
    Enum.reduce(List.flatten(board),0,fn number, acc ->
      if number != "x", do: acc + String.to_integer(number), else: acc
    end)
  end

  defp checkWinners(boards) do
    Enum.reduce(boards,[], fn board, acc ->
      win = Enum.reduce_while(board, nil, fn line, acc2 ->
        if Enum.all?(line, fn x -> x == "x" end), do: {:halt, board}, else: {:cont, acc2}
      end)

      zipped = Enum.zip(board)
      winvertical = Enum.reduce_while(zipped,nil, fn vline, acc2 ->
        if Enum.all?(Tuple.to_list(vline), fn x -> x == "x" end), do: {:halt, board}, else: {:cont, acc2}
      end)

      if win != nil || winvertical != nil, do: acc ++ [board], else: acc
    end)
  end

  defp markNumbers(boards, newNumber) do
    boards
    |> Enum.map(fn board ->
      Enum.map(board, fn row ->
        Enum.map(row, fn number ->
          if number == newNumber, do: "x", else: number
        end)
      end)
    end)
  end

  defp parse(data) do
    [numbers | tail] = String.split(data,"\n", trim: true)
    boards = Enum.chunk_every(tail, 5)
    |> Enum.map(fn x ->
      Enum.map(x,fn y ->
        String.split(y," ", trim: true)
      end)
    end)
    %{numbers: String.split(numbers,",", trim: true), boards: boards}
  end
end


Day4.part1()
Day4.part2()
