defmodule Day1 do
  def part1() do
    {:ok, contents} = File.read("input")
    contents
    |> parse
    |> Enum.reduce(%{last: nil, counter: 0},fn x, acc ->
      cond do
        acc.last < x -> %{last: x, counter: acc.counter + 1 }
        true -> %{last: x, counter: acc.counter}
      end
    end)
    |> Map.pop(:counter)
    |> elem(0)
    |> IO.puts()
  end


  def part2() do
    {:ok, contents} = File.read("input")
    contents
    |> parse
    |> slidingWindows
    |> Map.pop(:counter)
    |> elem(0)
    |> IO.puts()
  end

  defp parse(data) do
    String.split(data,"\n", trim: true)
    |> Enum.map(fn x -> String.to_integer(x) end)
  end


  defp slidingWindows(data) do
    Enum.reduce(Enum.slice(data,3..-1),%{window: Enum.take(data,3), counter: 0},fn x, acc ->
      newwindow = tl(acc.window) ++ [x]
      cond do
        Enum.sum(newwindow) > Enum.sum(acc.window) -> %{window: newwindow, counter: acc.counter + 1 }
        true -> %{window: newwindow, counter: acc.counter}
      end
    end)
  end
end

Day1.part1()
Day1.part2()
