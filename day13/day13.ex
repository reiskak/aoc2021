defmodule Day13 do

  def part1 do
    data = readData()
    fold(data.dotmap, [hd(data.commands)])
    |> MapSet.size()
    |> IO.inspect()
  end

  def part2 do
    data = readData()
    fold(data.dotmap, data.commands)
    |> print()
  end


  defp fold(dotmap, commands) do
    if(length(commands) == 0) do
      dotmap
    else
      {axis, row} = hd(commands)
      idx = case axis do
        "x" -> 0
        "y" -> 1
      end
      Enum.filter(dotmap, fn x ->
        elem(x,idx) > row
      end)
      |> Enum.reduce(dotmap, fn x, acc ->
        MapSet.delete(acc, x)
        |> MapSet.put(put_elem(x,idx, abs(elem(x,idx) - row*2)))
      end)
      |> fold(tl(commands))
    end
  end

  defp print(data) do
    {maxX, _} = Enum.max_by(data,fn {x,y} ->
      x
    end)
    {_, maxY} = Enum.max_by(data,fn {x,y} ->
      y
    end)
    Enum.each(0..maxY,fn y ->
      Enum.each(0..maxX, fn x ->
        char = if MapSet.member?(data,{x,y}), do: "#", else: "."
        IO.write(char)
      end)
      IO.write("\n")
    end)
  end

  defp readData() do
    {:ok, contents} = File.read("input")
    contents |> parse
  end

  defp parse(data) do
    result =
      String.split(data, "\r\n")
      |> Enum.reduce(%{dotmap: MapSet.new(), commands: [], mode: 0}, fn x, acc ->
        if x == "" do
          %{dotmap: acc.dotmap, commands: acc.commands, mode: 1}
        else
          case acc.mode do
            0 ->
              [xc,yc] = String.split(x, ",")
              %{dotmap: MapSet.put(acc.dotmap, {String.to_integer(xc),String.to_integer(yc)}), commands: acc.commands, mode: 0}
            1 ->
              [_,_,command] = String.split(x, " ")
              [axis, row] = String.split(command, "=")
              %{dotmap: acc.dotmap, commands: acc.commands ++ [{axis,String.to_integer(row)}], mode: 1}
          end
        end
      end)
      Map.delete(result, :mode)
  end
end

Day13.part1()
Day13.part2()
