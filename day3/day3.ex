defmodule Day3 do
  def part1() do
    {:ok, contents} = File.read("input")
    result = contents
    |> parse
    |> Enum.zip()
    |> Enum.map(fn x -> Enum.frequencies(Tuple.to_list(x)) end)
    |> Enum.reduce(%{gamma: "", epsilon: ""}, fn x, acc ->
      cond do
        x[48] > x[49] -> %{gamma: acc.gamma <> "1", epsilon: acc.epsilon <> "0"}
        true -> %{gamma: acc.gamma <> "0", epsilon: acc.epsilon <> "1"}
      end
    end)
    |> Enum.map(fn x -> String.to_integer(elem(x,1),2) end)
    IO.puts(Enum.at(result,0) * Enum.at(result,1))

  end

  def part2() do
    {:ok, contents} = File.read("input")
    parsed = contents
    |> parse

    oxygen = remove(parsed,0,&getLeastCommon/1)
    co2 = remove(parsed,0,&getMostCommon/1)
    IO.puts(oxygen*co2)
  end

  defp remove(data, idx, fnc) do
    zipped = List.zip(data)
    freq = Enum.frequencies(Tuple.to_list(Enum.at(zipped,idx)))
    toRemove = fnc.(freq)

    newData = Enum.filter(data,fn x ->
      Enum.at(x,idx) != toRemove
    end)

    if length(newData) > 1, do: remove(newData, idx + 1,fnc), else: String.to_integer(to_string(List.first(newData)),2)
  end

  defp getMostCommon(freq) do
    zeros = elem(Enum.at(freq,0),1)
    ones = elem(Enum.at(freq,1),1)
    cond do
      ones > zeros -> 49
      ones == zeros -> 49
      true -> 48
    end
  end

  defp getLeastCommon(freq) do
    zeros = elem(Enum.at(freq,0),1)
    ones = elem(Enum.at(freq,1),1)
    cond do
      ones > zeros -> 48
      ones == zeros -> 48
      true -> 49
    end
  end

  defp parse(data) do
    String.split(data,"\n", trim: true)
    |> Enum.map(fn x ->
      to_charlist(x)
    end)
  end
end


#Day3.part1()
Day3.part2()
