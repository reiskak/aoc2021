defmodule Day2 do
  def part1() do
    {:ok, contents} = File.read("input")
    result = contents
    |> parse
    |> Enum.reduce(%{position: 0, depth: 0}, fn x, acc ->
      case x.command do
        "down" -> %{position: acc.position, depth: acc.depth + x.amount}
        "up" -> %{position: acc.position, depth: acc.depth - x.amount}
        "forward" -> %{position: acc.position + x.amount, depth: acc.depth}
      end
    end)
    IO.inspect(result[:depth]*result[:position])
  end

  def part2() do
    {:ok, contents} = File.read("input")
    result = contents
    |> parse
    |> Enum.reduce(%{position: 0, depth: 0, aim: 0}, fn x, acc ->
      case x.command do
        "down" -> %{position: acc.position, depth: acc.depth, aim: acc.aim + x.amount}
        "up" -> %{position: acc.position, depth: acc.depth, aim: acc.aim - x.amount}
        "forward" -> %{position: acc.position + x.amount, depth: acc.depth + acc.aim * x.amount, aim: acc.aim}
      end
    end)
    IO.inspect(result[:depth]*result[:position])
  end


  defp parse(data) do
    String.split(data,"\n", trim: true)
    |> Enum.map(fn x ->
       [command, amount] = String.split(x, " ")
       %{command: command, amount: String.to_integer(amount)}
       end)
  end
end

Day2.part1()
Day2.part2()
