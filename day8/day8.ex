defmodule Day8 do
  def part1() do
    data = readData()
    |> Enum.reduce(0,fn [patterns | output], outerCounter ->
      outerCounter + Enum.reduce(hd(output),0, fn x, innerCounter ->
        if String.length(x) in [2,3,4, 7], do: innerCounter + 1, else: innerCounter
      end)
    end)
    |> IO.inspect()
  end

  def part2() do
    data = readData()
    |> Enum.reduce(0,fn [patterns | output], outerCounter ->
      sorted = Enum.map(patterns, fn x ->
        Enum.sort(String.to_charlist(x))
      end)
      %{2 => [one], 3 => [seven], 4 => [four], 5 => fives, 6 => sixs, 7 => [eight]} = Enum.group_by(sorted, &length/1)
      # Solve numbers with six letters
      nine = findLetter(sixs,four)
      zero = findLetter(sixs -- [nine], seven)
      [six] = sixs -- [nine, zero]
      # Solve numbers with five letters
      three = findLetter(fives, seven)
      five = Enum.find(fives, fn x ->
         length(six -- x) == 1
        end)
      [two] = fives -- [three,five

      # Map strings to numeric values
      numberMap = %{
        zero => "0",
        one => "1",
        two => "2",
        three => "3",
        four => "4",
        five => "5",
        six => "6",
        seven => "7",
        eight => "8",
        nine => "9"
      }
      sortedOutput = Enum.map(hd(output), fn x ->
        Enum.sort(String.to_charlist(x))
      end)

      outerCounter + String.to_integer(Enum.reduce(sortedOutput,"", fn x, innerCounter ->
        innerCounter <> Map.get(numberMap, x)
      end))
    end)
    |> IO.inspect()
  end

  defp findLetter(letterList, letterToCompare) do
    Enum.find(letterList,fn x ->
      letterToCompare -- x == []
    end)
  end

  defp readData() do
    {:ok, contents} = File.read("input.txt")
    contents |> parse
  end

  defp parse(data) do
    String.split(data,"\r\n", trim: true)
    |> Enum.map(fn x ->
      String.split(x," | ",trim: true)
      |> Enum.map(fn y ->
        String.split(y, " ")
      end)
    end)
  end
end

Day8.part1()
Day8.part2()
