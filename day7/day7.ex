defmodule Day7 do
  def part1() do
    data = readData() |> Enum.sort()
    median = Enum.at(data, div(length(data), 2))
    Enum.reduce(data,0, fn x, acc ->
      acc + abs(x - median)
    end)
    |> IO.inspect()
  end

  def part2() do
    data = readData()
    avg = div(Enum.sum(data), length(data))
    Enum.reduce(data,0, fn x, acc ->
      acc + Enum.reduce(1..abs(x - avg),0, fn n, acc2 ->
        acc2 + n
      end)
    end)
    |> IO.inspect()
  end

  defp readData() do
    {:ok, contents} = File.read("input")
    contents |> parse
  end

  defp parse(data) do
    String.split(data,",",trim: true)
    |> Enum.map(fn x ->
      String.to_integer(x)
    end)
  end
end

Day7.part1()
Day7.part2()
