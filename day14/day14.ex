defmodule Day14 do
  def part(n) do
    data = readData()

    initdata = Enum.chunk_every(data.template,2, 1, :discard)
    |> Enum.reduce(%{}, fn x, acc ->
      Map.update(acc,x,1,fn amt -> amt + 1 end)
    end) # Make "Sliding window" from template string

    values = Enum.reduce(1..n, initdata, fn _, acc ->
      reduce(acc, data.rules) # loop n times
    end)
    |> Enum.reduce(%{},fn {k,v}, acc ->
      Map.update(acc,hd(k),v, fn amt -> amt + v end) # construct map of char frequencies from sliding window pairs
    end)
    |> Map.update(List.last(data.template),1, fn x -> x + 1 end) # Add the missing last character
    |> Map.values() # Get only values

    IO.inspect(Enum.max(values) - Enum.min(values))
  end

  defp reduce(initialdata, rules) do
    Enum.reduce(initialdata, initialdata, fn {k, v}, acc -> # loop all pairs in sliding window
      if v == 0 do
        acc # if 0 continue
      else
        Map.get(rules,k,[]) # get new pair from rules based on current window
        |> Enum.reduce(acc, fn x, acc2 ->
          Map.update(acc2,x,v,fn amt -> amt + v end) # update map with new pairs
        end)
        |> Map.update!(k, fn x -> x - v end) # remove original pair
      end
    end)
  end

  defp readData() do
    {:ok, contents} = File.read("input")
    contents |> parse
  end

  defp parse(data) do
    result =
      String.split(data, "\r\n")
      |> Enum.reduce(%{template: "", rules: %{}, mode: 0}, fn x, acc ->
        if x == "" do
          %{template: acc.template, rules: acc.rules, mode: 1}
        else
          case acc.mode do
            0 ->
              %{template: String.graphemes(x), rules: acc.rules, mode: 0}
            1 ->
              [rule,result] = String.split(x, " -> ")
              [a,b] = String.graphemes(rule)
              %{template: acc.template, rules: Map.put(acc.rules, [a,b] , [[a, result],[result, b]]) , mode: 1}
          end
        end
      end)
      Map.delete(result, :mode)
  end
end


Day14.part(10)
Day14.part(40)
