defmodule Day5 do
  def part1() do
    {:ok, contents} = File.read("input")
    data = contents |> parse
    board = %{}
    Enum.reduce(data,board,fn line, acc ->
      drawLine(acc,line.startpoint, line.endpoint)
    end)
    #|> printMap()
    |> Enum.reduce(0,fn {_k,v}, acc ->
      if v > 1, do: acc + 1, else: acc
    end)
    |> IO.inspect()
  end

  defp printMap(board) do
    Enum.each(0..9, fn ver ->
      Enum.each(0..9, fn hor ->
        if Map.has_key?(board, {hor,ver}), do: IO.write(Map.get(board, {hor,ver})), else: IO.write(".")
      end)
      IO.write("\n")
    end)
  end

  defp drawLine(board, startPoint, endPoint) do
    startTuple = {startPoint.x, startPoint.y}
    if startPoint.x != endPoint.x and startPoint.y != endPoint.y do
      difference = endPoint.x - startPoint.x
      ymulti = if endPoint.y - startPoint.y != difference, do: -1, else: 1
      Enum.reduce(0..difference,board, fn x, acc ->
        tuple = {elem(startTuple,0) + x, elem(startTuple,1) + ymulti * x}
        updateBoard(acc, tuple)
      end)
    else
      directionIdx = if startPoint.x == endPoint.x, do:  1, else:  0
      difference = endPoint.x - startPoint.x + endPoint.y - startPoint.y
      Enum.reduce(0..difference,board, fn x, acc ->
        tuple = put_elem(startTuple,directionIdx,elem(startTuple,directionIdx) + x)
        updateBoard(acc, tuple)
      end)
    end
  end

  defp updateBoard(board,positionTuple) do
    if Map.has_key?(board, positionTuple), do: Map.update!(board,positionTuple, fn x -> x + 1 end) , else: Map.put(board,positionTuple, 1)
  end

  defp parse(data) do
    String.split(data,"\n")
    |> Enum.map(fn line ->
      [startp, endp] =String.split(line," -> ")
      |> Enum.map(fn values ->
        [x,y] = String.split(values,",")
        %{x: String.to_integer(x), y: String.to_integer(y) }
      end)
      %{startpoint: startp, endpoint: endp}
    end)
  end
end

Day5.part1()
