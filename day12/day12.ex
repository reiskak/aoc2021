defmodule Day12 do
  def part() do
    data = readData()

    traverse(data, "start", [], MapSet.new())
    |> IO.inspect()

    traversetwo(data, "start", [], %{})
    |> IO.inspect()
  end

  defp traverse(routes, curpoint, route, smallvisited) do
    if curpoint == "end" do
      1
    else
      Map.get(routes, curpoint, [])
      |> Enum.filter(fn x ->
        !MapSet.member?(smallvisited, x)
      end)
      |> Enum.reduce(0, fn junction, acc ->
        acc +
          traverse(
            routes,
            junction,
            route ++ [curpoint],
            if(islowercase(curpoint) == true,
              do: MapSet.put(smallvisited, curpoint),
              else: smallvisited
            )
          )
      end)
    end
  end

  defp traversetwo(routes, curpoint, route, smallvisited) do
    if curpoint == "end" do
      1
    else
      newsmallvisited =
        if islowercase(curpoint) == true,
          do: Map.update(smallvisited, curpoint, 1, fn x -> x + 1 end),
          else: smallvisited

      Map.get(routes, curpoint, [])
      |> Enum.filter(fn x ->
        cond do
          x == "start" -> false
          Enum.sum(Map.values(newsmallvisited)) == length(Map.values(newsmallvisited)) -> true
          Map.has_key?(newsmallvisited, x) -> false
          true -> true
        end
      end)
      |> Enum.reduce(0, fn junction, acc ->
        acc + traversetwo(routes, junction, route ++ [curpoint], newsmallvisited)
      end)
    end
  end

  defp readData() do
    {:ok, contents} = File.read("input")
    contents |> parse
  end

  defp islowercase(string), do: string == String.downcase(string)

  defp parse(data) do
    result =
      String.split(data, "\n", trim: true)
      |> Enum.reduce(%{}, fn x, acc ->
        [from, to] = String.split(x, "-")

        Map.put(acc, from, Map.get(acc, from, []) ++ [to])
        |> Map.put(to, Map.get(acc, to, []) ++ [from])
      end)
  end
end

Day12.part()
