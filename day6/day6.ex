defmodule Day6 do
  def part(n) do
    {:ok, contents} = File.read("input")
    data = contents |> parse
    freq = Enum.frequencies(data)
    Enum.reduce(1..n, freq, fn _, acc ->
      day(acc)
    end)
    |> Map.values()
    |> Enum.sum()
    |> IO.inspect()
  end

  defp day(data) do
    {zeros, rest} = Map.pop(data,0,0)
    Enum.map(rest, fn {k, v} ->
      {k-1 ,v}
    end)
    |> Enum.into(%{})
    |> Map.merge(%{6 => zeros, 8 => zeros}, fn _, x, y -> x + y end)
  end

  defp parse(data) do
    String.split(data,",",trim: true)
    |> Enum.map(fn x ->
      String.to_integer(x)
    end)
  end
end

Day6.part(80)
Day6.part(256)
